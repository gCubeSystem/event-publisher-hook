package org.gcube.portal.event.publisher.lr62.model;

import com.liferay.portal.ModelListenerException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.UserGroup;

public class UserGroupEventPublisher extends AbstractEventPublisherBaseModelListener<UserGroup> {

    protected static final Log log = LogFactoryUtil.getLog(UserGroupEventPublisher.class);

    public UserGroupEventPublisher() {
        super();
        log.info("New UserGroupEventPublisher instance created");
    }

    @Override
    public void onAfterCreate(UserGroup userGroup) throws ModelListenerException {
        log.info("onAfterCreate");
    }

    @Override
    public void onBeforeRemove(UserGroup userGroup) throws ModelListenerException {
        log.info("onBeforeRemove");
    }

    @Override
    public void onAfterAddAssociation(Object classPK, String associationClassName, Object associationClassPK)
            throws ModelListenerException {

        log.info("onAfterAddAssociation");
    }

    @Override
    public void onAfterRemove(UserGroup userGroupRole) throws ModelListenerException {
        log.info("onAfterRemove");
    }

    @Override
    public void onAfterRemoveAssociation(Object classPK, String associationClassName, Object associationClassPK)
            throws ModelListenerException {

        log.info("onAfterRemoveAssociation");
    }

    @Override
    public void onAfterUpdate(UserGroup userGroupRole) throws ModelListenerException {
        log.info("onAfterUpdate");
    }

    @Override
    public void onBeforeAddAssociation(Object classPK, String associationClassName, Object associationClassPK)
            throws ModelListenerException {

        log.info("onBeforeAddAssociation");
    }

    @Override
    public void onBeforeCreate(UserGroup userGroupRole) throws ModelListenerException {
        log.info("onBeforeCreate");
    }

    @Override
    public void onBeforeRemoveAssociation(Object classPK, String associationClassName, Object associationClassPK)
            throws ModelListenerException {

        log.info("onBeforeRemoveAssociation");
    }

    @Override
    public void onBeforeUpdate(UserGroup userGroupRole) throws ModelListenerException {
        log.info("onBeforeUpdate");
    }

}