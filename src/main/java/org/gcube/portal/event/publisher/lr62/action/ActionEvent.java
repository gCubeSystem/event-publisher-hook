package org.gcube.portal.event.publisher.lr62.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.gcube.portal.event.publisher.lr62.PortalEvent;
import org.gcube.vomanagement.usermanagement.exception.UserManagementSystemException;
import org.gcube.vomanagement.usermanagement.exception.UserRetrievalFault;

import com.liferay.portal.model.User;

public class ActionEvent extends PortalEvent {

    private static final long serialVersionUID = -256209939036712171L;

    public static final String LOGIN_NAME = "login";
    public static final String LOGOUT_NAME = "logout";

    private ActionEvent(String name, User user) throws UserManagementSystemException, UserRetrievalFault {
        super(name);
        setUser(user);
    }

    public static ActionEvent newLoginEvent(HttpServletRequest request, HttpServletResponse response) {
        User user = (User) request.getSession(false).getAttribute("USER");
        if (log.isDebugEnabled()) {
            log.debug("Sending login event for user: " + user.getScreenName());
        }
        try {
            return new ActionEvent(LOGIN_NAME, user);
        } catch (UserManagementSystemException | UserRetrievalFault e) {
            log.error("Cannot create action event for user: " + user, e);
            return null;
        }
    }

    public static ActionEvent newLogoutEvent(HttpServletRequest request, HttpServletResponse response) {
        User user = (User) request.getSession(false).getAttribute("USER");
        if (log.isDebugEnabled()) {
            log.debug("Sending logut event for user: " + user.getScreenName());
        }
        try {
            return new ActionEvent(LOGOUT_NAME, user);
        } catch (UserManagementSystemException | UserRetrievalFault e) {
            log.error("Cannot create action event for user: " + user, e);
            return null;
        }
    }

}
