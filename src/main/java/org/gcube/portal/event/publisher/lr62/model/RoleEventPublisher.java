package org.gcube.portal.event.publisher.lr62.model;

import com.liferay.portal.ModelListenerException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.RoleConstants;

public class RoleEventPublisher extends AbstractEventPublisherBaseModelListener<Role> {

    protected static final Log log = LogFactoryUtil.getLog(RoleEventPublisher.class);

    public RoleEventPublisher() {
        super();
        log.info("New RoleEventPublisher instance created");
    }

    @Override
    public void onAfterCreate(Role role) throws ModelListenerException {
        if (role.getType() == RoleConstants.TYPE_SITE) {
            log.info("Created a new role having site type");
            RoleEvent event = RoleEvent.newCreatedEvent(role);

            if (log.isTraceEnabled()) {
                log.trace("Event is: " + event);
            }
            publish(event);
        } else if (log.isDebugEnabled()) {
            log.debug("Created a non-site type role: " + role.getName());
        }
    }

    @Override
    public void onBeforeRemove(Role role) throws ModelListenerException {
        if (role.getType() == RoleConstants.TYPE_SITE) {
            log.info("Removed a role having site type");
            RoleEvent event = RoleEvent.newDeletedEvent(role);

            if (log.isTraceEnabled()) {
                log.trace("Event is: " + event);
            }
            publish(event);
        } else if (log.isDebugEnabled()) {
            log.debug("Removed a non-site type role: " + role.getTitle());
        }
    }

    @Override
    public void onAfterUpdate(Role role) throws ModelListenerException {
//        if (role.getType() == RoleConstants.TYPE_SITE) {
//            log.info("Updated a role having site type");
//            RoleEvent event = RoleEvent.newUpdatedEvent(role);
//
//            if (log.isTraceEnabled()) {
//                log.trace("Event is: " + event);
//            }
//            publish(event);
//        } else if (log.isDebugEnabled()) {
//            log.debug("Updated a non-site type role: " + role.getName());
//        }
    }

    /* Uninteresting model events */

    @Override
    public void onAfterAddAssociation(Object classPK, String associationClassName, Object associationClassPK)
            throws ModelListenerException {
    }

    @Override
    public void onAfterRemove(Role role) throws ModelListenerException {
    }

    @Override
    public void onAfterRemoveAssociation(Object classPK, String associationClassName, Object associationClassPK)
            throws ModelListenerException {
    }

    @Override
    public void onBeforeAddAssociation(Object classPK, String associationClassName, Object associationClassPK)
            throws ModelListenerException {
    }

    @Override
    public void onBeforeCreate(Role role) throws ModelListenerException {
    }

    @Override
    public void onBeforeRemoveAssociation(Object classPK, String associationClassName, Object associationClassPK)
            throws ModelListenerException {
    }
    
    @Override
    public void onBeforeUpdate(Role role) throws ModelListenerException {
    }

}
