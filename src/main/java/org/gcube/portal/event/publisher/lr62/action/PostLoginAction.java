package org.gcube.portal.event.publisher.lr62.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.gcube.event.publisher.EventPublisher;

import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public class PostLoginAction extends Action {

    protected static final Log log = LogFactoryUtil.getLog(PostLoginAction.class);

    private EventPublisher eventPublisher;

    public PostLoginAction() {
        eventPublisher = new ActionEventPublisher();
    }

    @Override
    public void run(HttpServletRequest request, HttpServletResponse response) throws ActionException {
        ActionEvent actionEvent = ActionEvent.newLoginEvent(request, response);
        if (log.isDebugEnabled()) {
            log.debug("ActionEvent is: " + actionEvent);
        }
        eventPublisher.publish(actionEvent);
    }

}
