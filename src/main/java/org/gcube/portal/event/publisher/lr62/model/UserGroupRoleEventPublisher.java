package org.gcube.portal.event.publisher.lr62.model;

import com.liferay.portal.ModelListenerException;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.Group;
import com.liferay.portal.model.UserGroupRole;

public class UserGroupRoleEventPublisher extends AbstractEventPublisherBaseModelListener<UserGroupRole> {

    protected static final Log log = LogFactoryUtil.getLog(UserGroupRoleEventPublisher.class);

    public UserGroupRoleEventPublisher() {
        super();
        log.info("New UserGroupRoleEventPublisher instance created");
    }

    @Override
    public void onAfterCreate(UserGroupRole userGroupRole) throws ModelListenerException {
        try {
            Group group = userGroupRole.getGroup();
            if (group.isSite() && group.isActive()) {
                log.debug("Created a site user's group role");
                UserGroupRoleEvent event = UserGroupRoleEvent.newCreatedEvent(userGroupRole, userGroupRole.getUser(),
                        group, userGroupRole.getRole());

                if (log.isTraceEnabled()) {
                    log.trace("Event is: " + event);
                }
                publish(event);
            } else if (log.isDebugEnabled() && !group.isSite()) {
                log.debug("Created a non-site user's group role");
            } else if (log.isDebugEnabled() && group.isSite() && !group.isActive()) {
                log.debug("Created a user's group role for a disabled group");
            }
        } catch (PortalException | SystemException e) {
            log.error("Cannot get related model objects", e);
        }

    }

    @Override
    public void onBeforeRemove(UserGroupRole userGroupRole) throws ModelListenerException {
        try {
            Group group = userGroupRole.getGroup();
            if (group.isSite() && group.isActive()) {
                log.debug("Deleted a site user's group role");
                UserGroupRoleEvent event = UserGroupRoleEvent.newDeletedEvent(userGroupRole, userGroupRole.getUser(),
                        group, userGroupRole.getRole());

                if (log.isTraceEnabled()) {
                    log.trace("Event is: " + event);
                }
                publish(event);
            } else if (log.isDebugEnabled() && !group.isSite()) {
                log.debug("Deleted a non-site user's group role");
            } else if (log.isDebugEnabled() && group.isSite() && !group.isActive()) {
                log.debug("Deleted a user's group role for a disabled group");
            }
        } catch (PortalException | SystemException e) {
            log.error("Cannot get related model objects", e);
        }
    }

    /* Uninteresting model events */

    @Override
    public void onAfterAddAssociation(Object classPK, String associationClassName, Object associationClassPK)
            throws ModelListenerException {
    }

    @Override
    public void onAfterRemove(UserGroupRole userGroupRole) throws ModelListenerException {
    }

    @Override
    public void onAfterRemoveAssociation(Object classPK, String associationClassName, Object associationClassPK)
            throws ModelListenerException {
    }

    @Override
    public void onAfterUpdate(UserGroupRole userGroupRole) throws ModelListenerException {
    }

    @Override
    public void onBeforeAddAssociation(Object classPK, String associationClassName, Object associationClassPK)
            throws ModelListenerException {
    }

    @Override
    public void onBeforeCreate(UserGroupRole userGroupRole) throws ModelListenerException {
    }

    @Override
    public void onBeforeRemoveAssociation(Object classPK, String associationClassName, Object associationClassPK)
            throws ModelListenerException {
    }

    @Override
    public void onBeforeUpdate(UserGroupRole userGroupRole) throws ModelListenerException {
    }

}
