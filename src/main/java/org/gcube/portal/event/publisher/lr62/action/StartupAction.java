package org.gcube.portal.event.publisher.lr62.action;

import org.gcube.event.publisher.EventPublisher;

import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.events.SimpleAction;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public class StartupAction extends SimpleAction {

    protected static final Log log = LogFactoryUtil.getLog(StartupAction.class);

    private EventPublisher eventPublisher;

    public StartupAction() {
        eventPublisher = new ActionEventPublisher();
    }

    @Override
    public void run(String[] ids) throws ActionException {
        SimpleActionEvent event = SimpleActionEvent.newStartupEvent(ids);
        if (log.isInfoEnabled()) {
            log.info("Sending startup event: " + event);
        }
        eventPublisher.publish(event);
    }

}
