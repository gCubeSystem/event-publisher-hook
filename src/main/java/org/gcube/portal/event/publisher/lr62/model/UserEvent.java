package org.gcube.portal.event.publisher.lr62.model;

import com.liferay.portal.model.Group;
import com.liferay.portal.model.User;

public class UserEvent extends PortalModelListenerEvent<User> {

    private static final long serialVersionUID = 3939268094440017646L;

    public static final String CREATED_NAME = "user_created";
    public static final String DELETED_NAME = "user_deleted";

    public static final String UG_CREATED_NAME = "user-group_created";
    public static final String UG_DELETED_NAME = "user-group_deleted";

    private UserEvent(String name, User user) {
        super(name, user, user);
    }

    private UserEvent(String name, User user, Group group) {
        this(name, user);
        setGroup(group);
    }

    public static UserEvent newCreatedEvent(User user) {
        return new UserEvent(CREATED_NAME, user);
    }

    public static UserEvent newDeletedEvent(User user) {
        return new UserEvent(DELETED_NAME, user);
    }

    public static UserEvent newCreatedEvent(User user, Group group) {
        return new UserEvent(UG_CREATED_NAME, user, group);
    }

    public static UserEvent newDeletedEvent(User user, Group group) {
        return new UserEvent(UG_DELETED_NAME, user, group);
    }

}
