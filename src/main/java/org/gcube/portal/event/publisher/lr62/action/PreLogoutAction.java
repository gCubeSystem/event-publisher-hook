package org.gcube.portal.event.publisher.lr62.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.gcube.event.publisher.EventPublisher;

import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public class PreLogoutAction extends Action {

    protected static final Log log = LogFactoryUtil.getLog(PreLogoutAction.class);

    private EventPublisher eventPublisher;

    public PreLogoutAction() {
        eventPublisher = new ActionEventPublisher();
    }

    @Override
    public void run(HttpServletRequest request, HttpServletResponse response) throws ActionException {
        ActionEvent actionEvent = ActionEvent.newLogoutEvent(request, response);
        if (log.isDebugEnabled()) {
            log.debug("ActionEvent is: " + actionEvent);
        }
        eventPublisher.publish(actionEvent);
    }

}
