package org.gcube.portal.event.publisher.lr62.model;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Role;

public class RoleEvent extends PortalModelListenerEvent<Role> {

    private static final long serialVersionUID = 3939268094440017646L;

    public static final String CREATED_NAME = "role_created";
    public static final String DELETED_NAME = "role_deleted";
//    public static final String UPDATED_NAME = "role_updated";

    private RoleEvent(String name, Role role) throws PortalException, SystemException {
        super(name, role);
        setRole(role);
    }

    public static RoleEvent newCreatedEvent(Role role) {
        try {
            return new RoleEvent(CREATED_NAME, role);
        } catch (PortalException | SystemException e) {
            log.error("Cannot create event from Role model object", e);
            return null;
        }
    }

    public static RoleEvent newDeletedEvent(Role role) {
        try {
            return new RoleEvent(DELETED_NAME, role);
        } catch (PortalException | SystemException e) {
            log.error("Cannot create event from Role model object", e);
            return null;
        }
    }

//    public static RoleEvent newUpdatedEvent(Role role) {
//        try {
//            return new RoleEvent(UPDATED_NAME, role.getDescriptiveName());
//        } catch (PortalException | SystemException e) {
//            log.error("Cannot create event from Role model object", e);
//            return null;
//        }
//    }

}