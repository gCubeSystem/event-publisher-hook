package org.gcube.portal.event.publisher.lr62.model;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Group;

public class GroupEvent extends PortalModelListenerEvent<Group> {

    private static final long serialVersionUID = 3939268094440017646L;

    public static final String CREATED_NAME = "group_created";
    public static final String DELETED_NAME = "group_deleted";

    private GroupEvent(String name, Group group) throws PortalException, SystemException {
        super(name, group);
        setGroup(group);
    }

    public static GroupEvent newCreatedEvent(Group group) {
        try {
            return new GroupEvent(CREATED_NAME, group);
        } catch (PortalException | SystemException e) {
            log.error("Cannot create event from Group model object", e);
            return null;
        }
    }

    public static GroupEvent newDeletedEvent(Group group) {
        try {
            return new GroupEvent(DELETED_NAME, group);
        } catch (PortalException | SystemException e) {
            log.error("Cannot create event from Group model object", e);
            return null;
        }
    }

}