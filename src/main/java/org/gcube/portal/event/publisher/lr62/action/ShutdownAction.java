package org.gcube.portal.event.publisher.lr62.action;

import org.gcube.event.publisher.EventPublisher;

import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.events.SimpleAction;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public class ShutdownAction extends SimpleAction implements Runnable {

    protected static final Log log = LogFactoryUtil.getLog(ShutdownAction.class);

    private EventPublisher eventPublisher;

    public ShutdownAction() {
        eventPublisher = new ActionEventPublisher();
//        Runtime.getRuntime().addShutdownHook(new Thread(this));
    }

    @Override
    public void run(String[] ids) throws ActionException {
        if (log.isInfoEnabled()) {
            log.info("Sending shutdown event");
        }
        eventPublisher.publish(SimpleActionEvent.newShutdownEvent(ids));
    }

    @Override
    public void run() {
        try {
            this.run(null);
        } catch (ActionException e) {
            // Left intentionally as it is
            e.printStackTrace();
        }
    }
}
