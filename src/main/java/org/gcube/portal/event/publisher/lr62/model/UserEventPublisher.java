package org.gcube.portal.event.publisher.lr62.model;

import com.liferay.portal.ModelListenerException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.Group;
import com.liferay.portal.model.User;
import com.liferay.portal.service.GroupLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;

public class UserEventPublisher extends AbstractEventPublisherBaseModelListener<User> {

    protected static final Log log = LogFactoryUtil.getLog(UserEventPublisher.class);

    public UserEventPublisher() {
        super();
        log.info("New UserEventPublisher instance created");
    }

    @Override
    public void onAfterCreate(User user) throws ModelListenerException {
        log.info("New user created");
        UserEvent userEvent = UserEvent.newCreatedEvent(user);
        if (log.isTraceEnabled()) {
            log.trace("User event is: " + userEvent);
        }
        publish(userEvent);
    }

    @Override
    public void onBeforeRemove(User user) throws ModelListenerException {
        log.info("User removed");
        UserEvent userEvent = UserEvent.newDeletedEvent(user);
        if (log.isTraceEnabled()) {
            log.trace("User event is: " + userEvent);
        }
        publish(userEvent);
    }

    @Override
    public void onAfterAddAssociation(Object classPK, String associationClassName, Object associationClassPK)
            throws ModelListenerException {

        if (Group.class.getName().equals(associationClassName)) {
            log.debug("User has been associated to a group");
            User user = getUser((Long) classPK);
            Group group = getGroup((Long) associationClassPK);
            if (user != null && group != null) {
                if (group.isSite() && group.isActive()) {
                    if (log.isDebugEnabled()) {
                        log.debug("Target group is a site: " + group.getName());
                    }
                    UserEvent userEvent = UserEvent.newCreatedEvent(user, group);
                    if (log.isTraceEnabled()) {
                        log.trace("User event is: " + userEvent);
                    }
                    publish(userEvent);
                } else if (log.isDebugEnabled() && !group.isSite()) {
                    log.debug("Target Group is a non-site group");
                } else if (log.isDebugEnabled() && group.isSite() && !group.isActive()) {
                    log.debug("Target group is disabled");
                }
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("Uninteresting association with: " + associationClassName);
            }
        }
    }

    @Override
    public void onBeforeRemoveAssociation(Object classPK, String associationClassName, Object associationClassPK)
            throws ModelListenerException {

        if (Group.class.getName().equals(associationClassName)) {
            log.debug("User has been associated to a group");
            User user = getUser((Long) classPK);
            Group group = getGroup((Long) associationClassPK);
            if (user != null && group != null) {
                if (group.isSite() && group.isActive()) {
                    if (log.isDebugEnabled()) {
                        log.debug("Target group is a site: " + group.getName());
                    }
                    UserEvent userEvent = UserEvent.newDeletedEvent(user, group);
                    if (log.isTraceEnabled()) {
                        log.trace("User event is: " + userEvent);
                    }
                    publish(userEvent);
                } else if (log.isDebugEnabled() && !group.isSite()) {
                    log.debug("Target group is a non-site group");
                } else if (log.isDebugEnabled() && group.isSite() && !group.isActive()) {
                    log.debug("Target group is disabled");
                }
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("Uninteresting association removal with: " + associationClassName);
            }
        }
    }

    protected User getUser(Long userPK) {
        User user = null;
        try {
            if (log.isTraceEnabled()) {
                log.trace("Getting user object idetified by pk: " + userPK);
            }
            user = UserLocalServiceUtil.getUser(userPK);
        } catch (PortalException | SystemException e) {
            logger.error("Cannot get user with pk: " + userPK, e);
        }
        return user;
    }

    protected Group getGroup(Long groupPK) {
        Group group = null;
        try {
            if (log.isTraceEnabled()) {
                log.trace("Getting group object idetified by pk: " + groupPK);
            }
            group = GroupLocalServiceUtil.getGroup((Long) groupPK);
        } catch (PortalException | SystemException e) {
            logger.error("Cannot get group with pk: " + groupPK, e);
        }
        return group;
    }

    /* Uninteresting model events */

    @Override
    public void onAfterRemove(User user) throws ModelListenerException {
    }

    @Override
    public void onAfterRemoveAssociation(Object classPK, String associationClassName, Object associationClassPK)
            throws ModelListenerException {
    }

    @Override
    public void onAfterUpdate(User user) throws ModelListenerException {
    }

    @Override
    public void onBeforeAddAssociation(Object classPK, String associationClassName, Object associationClassPK)
            throws ModelListenerException {
    }

    @Override
    public void onBeforeCreate(User user) throws ModelListenerException {
    }

    @Override
    public void onBeforeUpdate(User user) throws ModelListenerException {
    }

}
