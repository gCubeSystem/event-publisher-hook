package org.gcube.portal.event.publisher.lr62.model;

import com.liferay.portal.ModelListenerException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.UserGroupGroupRole;

public class UserGroupGroupRoleEventPublisher extends AbstractEventPublisherBaseModelListener<UserGroupGroupRole> {

    protected static final Log log = LogFactoryUtil.getLog(UserGroupGroupRoleEventPublisher.class);

    public UserGroupGroupRoleEventPublisher() {
        super();
        log.info("New UserGroupGroupRoleEventPublisher instance created");
    }

    @Override
    public void onAfterCreate(UserGroupGroupRole userGroupGroupRole) throws ModelListenerException {
        log.info("onAfterCreate");
    }

    @Override
    public void onBeforeRemove(UserGroupGroupRole userGroupGroupRole) throws ModelListenerException {
        log.info("onBeforeRemove");
    }

    @Override
    public void onAfterAddAssociation(Object classPK, String associationClassName, Object associationClassPK)
            throws ModelListenerException {

        log.info("onAfterAddAssociation: "+ associationClassName);
    }

    @Override
    public void onAfterRemove(UserGroupGroupRole userGroupGroupRole) throws ModelListenerException {
        log.info("onAfterRemove");
    }

    @Override
    public void onAfterRemoveAssociation(Object classPK, String associationClassName, Object associationClassPK)
            throws ModelListenerException {

        log.info("onAfterRemoveAssociation: " + associationClassName);
    }

    @Override
    public void onAfterUpdate(UserGroupGroupRole userGroupGroupRole) throws ModelListenerException {
        log.info("onAfterUpdate");
    }

    @Override
    public void onBeforeAddAssociation(Object classPK, String associationClassName, Object associationClassPK)
            throws ModelListenerException {

        log.info("onBeforeAddAssociation: " + associationClassName);
    }

    @Override
    public void onBeforeCreate(UserGroupGroupRole userGroupGroupRole) throws ModelListenerException {
        log.info("onBeforeCreate");
    }

    @Override
    public void onBeforeRemoveAssociation(Object classPK, String associationClassName, Object associationClassPK)
            throws ModelListenerException {

        log.info("onBeforeRemoveAssociation: "+ associationClassName);
    }

    @Override
    public void onBeforeUpdate(UserGroupGroupRole userGroupGroupRole) throws ModelListenerException {
        log.info("onBeforeUpdate");
    }

}