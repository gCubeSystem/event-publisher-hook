package org.gcube.portal.event.publisher.lr62.model;

import org.gcube.portal.event.publisher.lr62.AbstractLR62EventPublisher;

import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.ModelListener;

public abstract class AbstractEventPublisherBaseModelListener<T extends BaseModel<T>>
        extends AbstractLR62EventPublisher
        implements ModelListener<T> {

    public AbstractEventPublisherBaseModelListener() {
        super();
    }

}
