package org.gcube.portal.event.publisher.lr62.action;

import org.gcube.portal.event.publisher.lr62.PortalEvent;

public class SimpleActionEvent extends PortalEvent {

    private static final long serialVersionUID = -256209939036712171L;

    public static final String STARTUP_NAME = "startup";
    public static final String SHUTDOWN_NAME = "shutdown";

    public static final String IDS_ENTRY = "ids";

    private SimpleActionEvent(String name, String[] ids) {
        super(name);
        setData(ids);
    }

    public static SimpleActionEvent newStartupEvent(String[] ids) {
        return new SimpleActionEvent(STARTUP_NAME, ids);
    }

    public static SimpleActionEvent newShutdownEvent(String[] ids) {
        return new SimpleActionEvent(SHUTDOWN_NAME, ids);
    }

    public void setData(String[] ids) {
        set(IDS_ENTRY, String.join(", ", ids));
    }

}
