package org.gcube.portal.event.publisher.lr62.model;

import java.util.Map;

import org.gcube.portal.event.publisher.lr62.PortalEvent;

import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.User;

public class PortalModelListenerEvent<T extends BaseModel<T>> extends PortalEvent {

    private static final long serialVersionUID = -9062395669075373612L;

    public static final String MODELCLASSNAME_ENTRY = "model-class-name";

    public PortalModelListenerEvent(String name, T baseModel) {
        super(name);
        setModelClassName(baseModel.getModelClassName());
    }

    public PortalModelListenerEvent(String name, User user, T baseModel) {
        super(name);
        setUser(user);
        setModelClassName(baseModel.getModelClassName());
    }

    public PortalModelListenerEvent(String name, Map<String, String> data, User user, T baseModel) {
        super(name, data);
        setUser(user);
        setModelClassName(baseModel.getModelClassName());
    }

    public void setModelClassName(String group) {
        set(MODELCLASSNAME_ENTRY, group);
    }

    public String getModelClassName() {
        return (String) get(MODELCLASSNAME_ENTRY);
    }

}
