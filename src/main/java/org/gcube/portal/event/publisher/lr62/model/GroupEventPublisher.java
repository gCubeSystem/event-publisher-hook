package org.gcube.portal.event.publisher.lr62.model;

import com.liferay.portal.ModelListenerException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.Group;

public class GroupEventPublisher extends AbstractEventPublisherBaseModelListener<Group> {

    protected static final Log log = LogFactoryUtil.getLog(GroupEventPublisher.class);

    public GroupEventPublisher() {
        super();
        log.info("New GroupEventPublisher instance created");
    }

    @Override
    public void onAfterCreate(Group group) throws ModelListenerException {
        if (group.isSite()) {
            log.info("Created a site Group");
            GroupEvent groupEvent = GroupEvent.newCreatedEvent(group);
            if (log.isTraceEnabled()) {
                log.trace("Group event is: " + groupEvent);
            }
            publish(groupEvent);
        } else if (log.isDebugEnabled()) {
            log.debug("Created a non-site Group");
        }
    }

    @Override
    public void onBeforeRemove(Group group) throws ModelListenerException {
        if (group.isSite()) {
            log.info("Deleted a site Group");
            GroupEvent groupEvent = GroupEvent.newDeletedEvent(group);
            if (log.isDebugEnabled()) {
                log.debug("Group event is: " + groupEvent);
            }
            publish(groupEvent);
        } else if (log.isDebugEnabled()) {
            log.debug("Deleted a non-site Group");
        }
    }

    /* Uninteresting model events */

    @Override
    public void onAfterRemove(Group group) throws ModelListenerException {
    }
    
    @Override
    public void onAfterAddAssociation(Object classPK, String associationClassName, Object associationClassPK)
            throws ModelListenerException {
    }

    @Override
    public void onAfterRemoveAssociation(Object classPK, String associationClassName, Object associationClassPK)
            throws ModelListenerException {
    }

    @Override
    public void onAfterUpdate(Group group) throws ModelListenerException {
    }

    @Override
    public void onBeforeAddAssociation(Object classPK, String associationClassName, Object associationClassPK)
            throws ModelListenerException {
    }

    @Override
    public void onBeforeCreate(Group group) throws ModelListenerException {
    }

    @Override
    public void onBeforeRemoveAssociation(Object classPK, String associationClassName, Object associationClassPK)
            throws ModelListenerException {
    }

    @Override
    public void onBeforeUpdate(Group group) throws ModelListenerException {
    }

}
