package org.gcube.portal.event.publisher.lr62.model;

import org.gcube.vomanagement.usermanagement.exception.UserManagementSystemException;
import org.gcube.vomanagement.usermanagement.exception.UserRetrievalFault;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Group;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.model.UserGroupRole;

public class UserGroupRoleEvent extends PortalModelListenerEvent<UserGroupRole> {

    private static final long serialVersionUID = 3939268094440017646L;

    public static final String CREATED_NAME = "user-group-role_created";
    public static final String DELETED_NAME = "user-group-role_deleted";

    private UserGroupRoleEvent(String name, UserGroupRole userGroupRole, User user, Group group, Role role)
            throws PortalException, SystemException, UserManagementSystemException, UserRetrievalFault {

        super(name, user, userGroupRole);
        setGroup(group);
        setRole(role);
    }

    public static UserGroupRoleEvent newCreatedEvent(UserGroupRole userGroupRole, User user, Group group, Role role) {
        try {
            return new UserGroupRoleEvent(CREATED_NAME, userGroupRole, user, group, role);
        } catch (PortalException | SystemException | UserManagementSystemException | UserRetrievalFault e) {
            log.error("Cannot create event from model object", e);
            return null;
        }
    }

    public static UserGroupRoleEvent newDeletedEvent(UserGroupRole userGroupRole, User user, Group group, Role role) {
        try {
            return new UserGroupRoleEvent(DELETED_NAME, userGroupRole, user, group, role);
        } catch (PortalException | SystemException | UserManagementSystemException | UserRetrievalFault e) {
            log.error("Cannot create event from model object", e);
            return null;
        }
    }

}