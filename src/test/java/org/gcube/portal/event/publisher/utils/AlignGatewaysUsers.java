package org.gcube.portal.event.publisher.utils;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLEncoder;

import org.gcube.event.publisher.BufferedEventProcessor;
import org.gcube.event.publisher.Event;
import org.gcube.event.publisher.EventProcessorException;
import org.gcube.portal.event.publisher.lr62.model.UserEvent;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Usage: <code>AlignGatewaysUsers &lt;conductor-endpoint> &lt;client-id> &lt;client-secret> &lt;keycloak-token-endpoint> &lt;json-export-resource> [&lt;gw-suffix>]</code><br>
 * 
 * 
 * The JSON input file is structured as "gateway and users" array, with users that belong to the gateway in an array.
 * Refer to the example below for further details.
 * 
 * <pre>
 * [
 *   {
 *       "gateway": "ATest Gateway",
 *       "users": [
 *           "gino.chino"
 *       ]
 *   },
 *   {
 *       "gateway": "A gCube Snapshot Gateway",
 *       "users": [
 *           "pino.lino",
 *           "gino.stilla",
 *           "cvcvcvcvcv"
 *       ]
 *   },
 *   [...]
 * ]
 * </pre>
 * 
 * @author Mauro Mugnaini
 *
 */
public class AlignGatewaysUsers {

    private static final Logger logger = LoggerFactory.getLogger(AlignGatewaysUsers.class);

    private static final String SENDER_ID = "gw2users-align";
    private static final String GW_SUFFIX = "+Gateway";

    @SuppressWarnings("unchecked")
    public AlignGatewaysUsers(String conductorEndpoint, String clientId, String clientSecret,
            String keycloakTokenEndpoint, String jsonExportResource, String gateway2export)
            throws IOException, ParseException, InterruptedException {

        BufferedEventProcessor processor = null;
        try {
            processor = new BufferedEventProcessor(conductorEndpoint, clientId,
                    clientSecret, keycloakTokenEndpoint, tobeSent -> {
                        String user = (String) tobeSent.get(UserEvent.USER_ENTRY);
                        String group = (String) tobeSent.get(UserEvent.GROUP_ENTRY);
                        return user + "@" + group;
                    });

        } catch (EventProcessorException e) {
            logger.error("Cannot create processor object. Exiting...", e);
            System.exit(1);
        }

        JSONArray export = (JSONArray) new JSONParser().parse(
                new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream(jsonExportResource)));

        long start = System.currentTimeMillis();
        for (int i = 0; i < export.size(); i++) {
            JSONObject entry = (JSONObject) export.get(i);
            String gateway = URLEncoder.encode((String) entry.get("gateway"), "UTF-8");
            if (gateway2export != null && !gateway.equals(gateway2export)) {
                continue;
            } else {
                logger.info("- Processing gateway: {}", gateway);
            }
            JSONArray usersArray = (JSONArray) entry.get("users");
            for (int j = 0; j < usersArray.size(); j++) {
                String user = (String) usersArray.get(j);
                // Group object is too difficult to implement as fake because is mapped via GroupManager
                // This is the workaround by using event base class directly:
                Event event = new Event(UserEvent.UG_CREATED_NAME, "manual", SENDER_ID);
                event.put(UserEvent.USER_ENTRY, user);
                event.put(UserEvent.GROUP_ENTRY, gateway);
                logger.debug(" * Offering: {}", user);
                processor.enqueueEvent(event);
            }
        }
        do {
            Thread.sleep(1000);
        } while (processor.allQueuedFinishedCorrectly());

        long elapsed = System.currentTimeMillis() - start;
        logger.info("\t\t\tFinished! Handled {} events in {} ms", processor.getCount(), elapsed);
        logger.info("\t\t\tErrors: {} - Stats per event: min: {} ms - max: {} ms.",
                new Object[] { processor.getErrors(), processor.getMin(), processor.getMax() });

        System.exit(0);
    }

    public static void main(String[] args) throws IOException, ParseException, InterruptedException {
        if (args.length < 5) {
            System.err.println(
                    "Not all arguments where provided.\nUsage: AlignGatewaysUsers <conductor-endpoint> <client-id> <client-secret> <keycloak-token-endpoint> <json-export-resource> [<gw-suffix>]");

            System.exit(1);
        }
        String conductorEndpoint = args[0];
        String clientId = args[1];
        String clientSecret = args[2];
        String keycloakTokenEndpoint = args[3];
        String jsonExportResource = args[4];
        new AlignGatewaysUsers(conductorEndpoint, clientId, clientSecret, keycloakTokenEndpoint, jsonExportResource,
                args.length == 6 ? args[5] + GW_SUFFIX : null);
    }

}