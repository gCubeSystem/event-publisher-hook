This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "event-publisher-portal"

## [v1.2.0-SNAPSHOT]
Added `AlignGatewaysUsers` and `SendContextUsersRoles` (#23628) utility functions.

## [v1.1.1]
`UserGroupRoleEventPublisher` don't send events if the group is not enabled (#21925)

## [v1.1.0]
Added new event publisher for new created and removed `Role`s having "`site`" type (#20896)

## [v1.0.0]
- First release (#19461)
